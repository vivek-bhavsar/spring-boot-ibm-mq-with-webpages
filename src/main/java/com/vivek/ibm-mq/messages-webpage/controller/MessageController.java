package com.luv2code.springboot.thymeleafdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@EnableJms
public class MessageController {

    @Autowired
    JmsTemplate jmsTemplate;

    private List<String> messages = new ArrayList<String>();

    @GetMapping("/send")
    public String showSendForm() {
        return "send-form";
    }

    @PostMapping("/send-message")
//    @ResponseBody
    public String send(String message, Model model) {
        try {
            jmsTemplate.convertAndSend("DEV.QUEUE.1", message);
            String text = "Message sent";
            model.addAttribute("text", text);
            return "redirect:/send";
        } catch(JmsException e) {
            e.printStackTrace();
            String text = "Failed! Message not sent";
            model.addAttribute("text", text);
            return "redirect:/send";
        }
    }

    @GetMapping("/receive")
    public String listMessages(Model theModel) {

        String theMessage = jmsTemplate.receiveAndConvert("DEV.QUEUE.1").toString();

        if (theMessage != null) {
            messages.add(theMessage);
        }

        // add to the Spring Model
        theModel.addAttribute("messages", messages);

        return "list-messages";
    }

    @RequestMapping("/clear")
    public String clearTable(Model theModel) {
        messages.clear();
        theModel.addAttribute("messages", messages);
        return "list-messages";
    }
}
